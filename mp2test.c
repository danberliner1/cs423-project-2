#include <stdio.h>
#include <stdlib.h>


// period range
#define MIN_PERIOD 50
#define MAX_PERIOD 500
#define MIN_COMP_TIME 500000

// the number of periods process does factorial calculation
#define MIN_ITER 20
#define MAX_ITER 100

long int factorial(unsigned long int arg);

// register a process to proc
void register_process(int pid, unsigned int period, unsigned int comp_time)
{
    printf("Registering %d\n", pid);
    char string[50];
    sprintf(string, "echo \"R,%d,%u,%u\" > /proc/mp2/status", pid, period, comp_time);
    printf("%s\n", string);
    system(string);
}

// send signal to proc that the application has finished its period
void yield(int pid){
    printf("Yield %d\n", pid);
    char string[50];
    sprintf(string, "echo \"Y,%d\" > /proc/mp2/status", pid);
    system(string);
}

// send signal to proc that the process is done.
void deregister(int pid){
    printf("Deregister %d\n", pid);
    char string[50];
    sprintf(string, "echo \"D,%d\" > /proc/mp2/status", pid);
    system(string);
}

// returns 1 if successfully registered
int is_registered(int pid)
{
    int is_registered = 0;
    char line[1024];
    FILE * file = fopen("/proc/mp2/status", "r");

    // one pid per line
    while(fgets(line, 1024, file) != NULL) 
    {
        int temp = atoi(line);
        if(temp == pid) 
        {
            printf("Registered PID: %d\n", temp);
            is_registered = 1;
            break;
        }
    }
    fclose(file);
    return is_registered;
}

unsigned int get_rand(unsigned int min, unsigned int max){
	unsigned int range = max-min+ 1 ;
	unsigned int rand_val = rand()%range;
	return rand_val+ min;
}

// Returns a random number between MIN_COMP_TIME and a fraction of period time
unsigned int get_comp_time(int period)
{
    unsigned int max_comp_time = (unsigned int)(period * 0.4 * 50000);
    if(max_comp_time < MIN_COMP_TIME)
    {
        max_comp_time = MIN_COMP_TIME;
    }

    return get_rand(MIN_COMP_TIME, max_comp_time)/100000;
}
						 
// Returns a random number between 50 and 500
unsigned int get_period()
{
    return get_rand(MIN_PERIOD, MAX_PERIOD);
}

// Returns a random number between MIN_ITERATIONS and MAX_ITERATIONS
unsigned int get_iter()
{
    return get_rand(MIN_ITER, MAX_ITER);
}

// main function
int main()
{
    // Seeding random number generator
    srand(time(NULL));

    int pid = getpid();
    unsigned int period = get_period();
    unsigned int comp_time = get_comp_time(period);

		//regitsering a process
    register_process(pid, period, comp_time);

		//checking if the process with pid is already registered
    if(!is_registered(pid))
    {
        printf("Fail to register %d\n", pid);
        exit(1);
    }
    printf("%d registered\n", pid);
	
    unsigned int iter = get_iter();

    		//yielding
    yield(pid);

    while(iter > 0)
    {
        struct timeval time;
        gettimeofday(&time);
        unsigned int start = time.tv_usec;

        unsigned int elapsed = 0;
        while(elapsed < comp_time*100000)
        {
            int counter;
            for(counter = 0; counter < 100; counter++)
            {
                factorial((counter % 20)+1);
            }
            gettimeofday(&time);
            elapsed = time.tv_usec - start;
        }

        yield(pid);
        iter--;
    }
    deregister(pid);
    return 0;
}

// Factorial Caculation function
long int factorial(unsigned long int arg)
{
    if(arg == 1)
        return 1;
    else 
			return arg * factorial(arg-1);
}
