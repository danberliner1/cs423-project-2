obj-m += mp2.o
all:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) modules
	gcc mp2test.c -o mp2test
clean:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) clean
