#define __KERNEL__
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/proc_fs.h>
#include <linux/string.h>
#include <linux/vmalloc.h>
#include <linux/klist.h>
#include <asm/uaccess.h>
#include <linux/timer.h>
#include <linux/interrupt.h>
#include <linux/rwsem.h>
#include <linux/spinlock.h>
#include <linux/slab.h>
#include <linux/kthread.h>
#include "mp2_given.h"
#define _BUF_SIZE 60

#define _STATE_READY 0
#define _STATE_YIELD 1
#define _STATE_RUN   2

MODULE_LICENSE("GPL");
struct rw_semaphore mr_rwsem;
struct rw_semaphore loop_sem;
int totCi;
int totPi;
int flag;
spinlock_t lock;

struct mp2_task_struct {
	struct task_struct* linux_task;
	struct timer_list wakeup_timer;
	int period;
	int computation;
	int state;
	int pid;
};
struct mp2_task_struct *current_mp2;

//Set up a list
LIST_HEAD(pid_list);

//We can't just insert mp2_task_struct variables. We have to do it this way.
struct pid_entry{
	struct mp2_task_struct *data;
	struct list_head list_member;
};

int divide_fixed3(int a, int b){
	return (1000*a)/b;
}

//Goes though and either remove a PID or updat eit
void worker_function(void *data){
	int debug=0;
	
	while(1){
		down_write(&loop_sem);

		printk("Loop\n");

		//Go though the list and find the higest ready task
		struct mp2_task_struct *highest_ready=NULL;
		int lowest_time=INT_MAX;
		down_write(&mr_rwsem);
		struct list_head *iter;
		struct pid_entry *obj_ptr;
		//	Go through all the items
		list_for_each(iter,&pid_list) {
			obj_ptr=list_entry(iter, struct pid_entry, list_member);
			//check the priority
		//	spin_lock_irqsave(&lock,flag);
			if(obj_ptr->data->state==_STATE_READY && obj_ptr->data->period < lowest_time){
				highest_ready=obj_ptr->data;
				obj_ptr->data->state = _STATE_RUN;
				lowest_time=obj_ptr->data->period;
			}
		//	spin_unlock_irqrestore(&lock,flag);
		}
		up_write(&mr_rwsem);

		//Take out the current process if there is one
		if(current_mp2){
			struct sched_param sparam;
			sparam.sched_priority=0;
			sched_setscheduler(current_mp2->linux_task, SCHED_NORMAL, &sparam);
			
			//spin_lock_irqsave(&lock,flag);
			current_mp2->state=_STATE_READY;
		//	spin_unlock_irqrestore(&lock,flag);

			current_mp2=NULL;
		}

		//If we found a process to put in
		if(highest_ready){

			//printk("Putting in process %i\n",highest_ready->linux_task->pid);

			struct sched_param sparam;
			wake_up_process(highest_ready->linux_task);
			sparam.sched_priority=MAX_USER_RT_PRIO-1;
			sched_setscheduler(highest_ready->linux_task, SCHED_FIFO, &sparam);
			current_mp2=highest_ready;
		}
	}	
}


//Dispatcher
void my_timer_callback(struct mp2_task_struct *target )
{
	//Set the process state to ready
	
	//spin_lock_irqsave(&lock,flag);
	target->state=_STATE_READY;
	//spin_unlock_irqrestore(&lock,flag);
	
	//Wake up dispach
	up_write(&loop_sem);

	printk("------------------------------------Timer firing off\n");
}
//Adds a char* to the list
void add_node(struct mp2_task_struct * arg, struct list_head *head){
	struct pid_entry *tmp_ptr = (struct pid_entry *)kmalloc(sizeof(struct pid_entry),GFP_KERNEL);
	
	down_write(&mr_rwsem);
	tmp_ptr->data = arg;
	INIT_LIST_HEAD(&tmp_ptr->list_member);
	list_add(&tmp_ptr->list_member,head);
	up_write(&mr_rwsem);
}
void delete_node(unsigned long pid){
	down_write(&mr_rwsem);
	struct list_head *iter, *n;
	struct pid_entry *obj_ptr;
	//	Go through all the items
	list_for_each_safe(iter,n,&pid_list) {
		obj_ptr=list_entry(iter, struct pid_entry, list_member);
		//De-register
		if(obj_ptr->data->pid == pid){
			totCi-=obj_ptr->data->computation;
			totPi-=obj_ptr->data->period; 
			kfree(iter);
//			kfree(obj_ptr);
		}
	}
	up_write(&mr_rwsem);

}
void yield_function(unsigned long pid, struct list_head *head){
	struct list_head *iter;
	struct pid_entry *obj_ptr;

	//traverse the linked list to find the process to be yield
	down_write(&mr_rwsem);
	list_for_each(iter,head){
		obj_ptr=list_entry(iter, struct pid_entry, list_member);
		if(obj_ptr->data->pid == pid){
			set_task_state(obj_ptr->data->linux_task, TASK_UNINTERRUPTIBLE);

			spin_lock_irqsave(&lock,flag);
			obj_ptr->data->state = _STATE_READY;
			spin_unlock_irqrestore(&lock,flag);
			//Have the timer set to wake up in period time
			mod_timer( &(obj_ptr->data->wakeup_timer), jiffies + msecs_to_jiffies(obj_ptr->data->period) );
		}
	}
	up_write(&mr_rwsem);
	//waking up the scheduler(dispatching thread)
	//worker_function(obj_ptr->data);	
}
//Iteratively clean a list
void clean_list(struct list_head *head){
	struct list_head *iter, *iter_last=NULL;
	struct pid_entry *obj_ptr;
	
	down_write(&mr_rwsem);
	list_for_each(iter,head) {
		kfree(iter_last);
		obj_ptr=list_entry(iter, struct pid_entry, list_member);
		kfree(obj_ptr);
		iter_last=iter;
	}
	up_write(&mr_rwsem);
}

//When client reads
int read_function( char *page, char **start, off_t off, 
    int count, int *eof, void *data){
	int len=0;
	int debug=0;
	struct list_head *iter, *iter_last=NULL;
	struct pid_entry *obj_ptr;
	down_read(&mr_rwsem);
	
	//Iterate though every item on the list and display information
	list_for_each(iter,&pid_list){
		obj_ptr=list_entry(iter, struct pid_entry, list_member);
		printk("PID: %i\n",obj_ptr->data->pid);
		len+=sprintf(page+len,"%i\n",obj_ptr->data->pid);
	}
	up_read(&mr_rwsem);

	return len;
}


//When client writes
ssize_t write_function( struct file *filp, const char __user *buf, 
	unsigned long len, void *data){
	static char *mem=NULL;
	//Copy the memory over
	mem=kmalloc(len+1,GFP_KERNEL);
	if(copy_from_user(mem,buf,len)){
		return -1;
	}
	//since we're dealing with a string we need to add a NULL terminator.
	mem[len]='\0';
	
	//Get the command and relevant PID
	
	//Store the starting positions of each piece
	char *tmp_ptrs[6];
	int i=0;
	char *main_ptr=mem;
	//This works under the assumption that the first and last character is never a comma
	int j;
	for(j=0;j<len-1;j++){
		if(mem[j]==','){
			mem[j]='\0';
			tmp_ptrs[i++]=mem+j+1;
		}
	}
	mem[j]='\0';

	char cmd=mem[0];
	pid_t pid=0;
	if(kstrtoint(tmp_ptrs[0],10,&pid)==0){}
	
	//Process by command
	if(cmd=='R'){
		if(i!=3){
			//Something went wrong, wrong number of arguments
			printk(KERN_INFO "Wrong params on cmd=R");
		} else {
			int debug=0;
			int period=0, computation=0;
			if(kstrtoint(tmp_ptrs[1],10,&period)==0){}
			if(kstrtoint(tmp_ptrs[2],10,&computation)==0){}	
			
			//Register the process
			struct mp2_task_struct *reg=kmalloc(sizeof(struct mp2_task_struct),GFP_KERNEL);
			reg->linux_task=pid_task(find_get_pid(pid), PIDTYPE_PID);
			reg->computation=computation;
			reg->period=period;
			
			spin_lock_irqsave(&lock,flag);
			reg->state=_STATE_READY;
			spin_unlock_irqrestore(&lock,flag);
			setup_timer( &(reg->wakeup_timer), my_timer_callback, reg );
			reg->pid=pid;

			if(divide_fixed3(totCi+computation,totPi+period)<=693){
				//Incriment the Ci and Pi totals
				totCi+=computation;
				totPi+=period; 

				//Add PID to the linked list
				add_node(reg,&pid_list);
				printk(KERN_INFO "Registering PID %i with period %i and computation %i\n",pid,period,computation);
			} else {
				printk(KERN_INFO "Failed to register PID %i | %i %i %i \n", pid,divide_fixed3(totCi+computation,totPi+period),computation,period);
			}

		}
	} else if(cmd=='Y'){
		if(i!=1){
			//someting went wrnog, wrong number of params
		} else {
			yield_function(pid, &pid_list);
			up_write(&loop_sem);
			printk(KERN_INFO "Yielding pid %i\n",pid);
		}
	} else if(cmd=='D'){
		if(i!=1){
			//Something went wrong, wrong number of params
		} else {
			delete_node(pid);

			printk(KERN_INFO "De-registering PID %i",pid);
		}

	} else {
		//Something went wrong
	}
	
	return len;
}


static struct proc_dir_entry *pde;
static struct proc_dir_entry *pde2;
int init_helloworld_module(void){
	spin_lock_init(&lock);
	init_rwsem(&mr_rwsem);
	init_rwsem(&loop_sem);
	//Create the timer
	totCi=0;
	totPi=0;
	
	current_mp2=NULL;
	//Create the proc entry
	pde2=proc_mkdir("mp2",NULL);
	pde = create_proc_entry("status",0666,pde2);
	if(pde){
		pde->read_proc=read_function;
		pde->write_proc=(write_proc_t*)write_function;
	}
	kthread_run(worker_function,NULL,"name");
	printk("End of init\n");
	return 0;
}

//Function to clean things up when we leave
void exit_helloworld_module(void){
	remove_proc_entry("status",pde);
	printk(KERN_INFO " Goodbye cruel world. Unloading.\n");
	clean_list(&pid_list);
}


module_init(init_helloworld_module);
module_exit(exit_helloworld_module);
